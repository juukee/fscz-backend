# fscz-backend

丰山初中后台管理系统-后端 基于eggjs的腾讯云云开发版本

[![Fscz CI](https://github.com/juukee/fscz-backend/workflows/Fscz%20CI/badge.svg)](https://github.com/juukee/fscz-backend/actions?query=workflow%3A%22Fscz+CI%22)
[![Travis](https://img.shields.io/travis/juukee/fscz-backend/master?label=Travis&style=flat)](https://travis-ci.org/juukee/fscz-backend)
[![AppVeyor](https://img.shields.io/appveyor/build/juukee/tcb-egg/master?label=Appveyor&style=flat)](https://ci.appveyor.com/project/juukee/tcb-egg)
[![codecov](https://codecov.io/gh/juukee/fscz-backend/branch/master/graph/badge.svg)](https://codecov.io/gh/juukee/fscz-backend)
[![GitHub package.json version](https://img.shields.io/github/package-json/v/juukee/tcb-egg)](https://github.com/juukee/fscz-backend)
[![GitHub repo size](https://img.shields.io/github/repo-size/juukee/fscz-backend)](https://github.com/juukee/fscz-backend)
[![GitHub](https://img.shields.io/github/license/juukee/fscz-backend)](https://github.com/juukee/fscz-backend)

## QuickStart

<!-- add docs here for user -->

see [egg docs][egg] for more detail.

### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### Deploy

```bash
$ npm start
$ npm stop
```

### npm scripts

- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.
- Use `npm run autod` to auto detect dependencies upgrade, see [autod](https://www.npmjs.com/package/autod) for more detail.


[egg]: https://eggjs.org
