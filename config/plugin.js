'use strict';

/** @type Egg.EggPlugin */
/** @type Egg.Alinode Node.js 性能平台（alinode）
 * https://node.console.aliyun.com/
 */

exports.alinode = {
  enable: true,
  package: 'egg-alinode',
  env: 'prod',
};
// 跨域
exports.cors = {
  enable: true,
  package: 'egg-cors',
};
