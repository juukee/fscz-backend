/* eslint valid-jsdoc: "off" */

'use strict';
const path = require('path');

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/

  const config = exports = {};
  const appRoot = appInfo.baseDir;
  const tcbDir = path.join(appRoot, '/logs');
  const alinodeLogdir = path.join(tcbDir, 'alinode');

  // alinode https://node.console.aliyun.com/
  config.alinode = {
    server: 'wss://agentserver.node.aliyun.com:8080',
    appid: '85057',
    secret: '53618f895af9dab49211b09c3ab73e3fe9f1a383',
    logdir: '/tmp',
    error_log: [
      path.join(alinodeLogdir, `${appInfo.pkg.name}/common-error.log`),
      path.join(alinodeLogdir, 'stderr.log'),
    ],
    packages: [
      path.join(appRoot, 'package.json'),
    ],
  };
  // 日志配置
  config.logger = {
    dir: tcbDir,
  };

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1603726177674_2236';

  // add your middleware config here
  config.middleware = [];


  config.security = {
    // domainWhiteList: [ 'scfscz.com' ], // 白名单
  };
  // 跨域设置
  config.cors = {
    origin: '*.scfscz.com,127.0.0.1:5000', // 匹配规则 域名+端口 *则为全匹配
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH',
    credentials: true,
  };

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
