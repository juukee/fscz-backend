'use strict';

module.exports = app => {
  console.log(app.config.env);
  // app.beforeStart(async () => {
  //   await app.model.sync({ alter: true, force: false });
  // });

  // app.once('server', server => {
  //   // websocket
  //   console.log(server);
  // });
  app.on('error', (err, ctx) => {
    // report error
    app.logger.warn(err, ctx);
  });
  app.on('request', ctx => {
    // console.log(ctx.request);
    app.logger.info(ctx.request.ip);
    app.logger.info(ctx.request.headers['user-agent']);
    // log receive request
  });
  app.on('response', ctx => {
    const used = Date.now() - ctx.starttime;
    app.logger.info('响应耗时 %d ms', used);
  });

};

