'use strict';

const Controller = require('egg').Controller;
// const CloudBase = require('@cloudbase/manager-node');


class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.body = 'hi, egg2';
  }
}

module.exports = HomeController;
