FROM ccr.ccs.tencentyun.com/tcb-913648653-qdkp/alinode AS builder
WORKDIR /app/fscz
COPY package.json /app/fscz/
RUN echo "https://mirrors.aliyun.com/alpine/v3.11/main/" > /etc/apk/repositories \
    && apk update \
    && apk add --no-cache bash \
    bash-doc \
    bash-completion \
    && rm -rf /var/cache/apk/* \
    && /bin/bash \
    && npm install --production --registry=https://registry.npm.taobao.org
COPY . /app/fscz

FROM ccr.ccs.tencentyun.com/tcb-913648653-qdkp/alinode
WORKDIR /app/fscz
COPY package.json /app/fscz/
RUN echo "https://mirrors.aliyun.com/alpine/v3.11/main/" > /etc/apk/repositories \
    && apk update \
    && apk add --no-cache bash \
    bash-doc \
    bash-completion \
    && rm -rf /var/cache/apk/* \
    && /bin/bash \
    && npm install --production --registry=https://registry.npm.taobao.org
COPY . /app/fscz
COPY --from=builder /app/fscz/node_modules ./node_modules
EXPOSE 7002
CMD ["npm", "run", "start"]