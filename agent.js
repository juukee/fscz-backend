'use strict';

class AppBootHook {

  constructor(app) {
    this.app = app;
  }
  // 配置文件即将加载，这是最后动态修改配置的时机
  configWillLoad() {
    // Ready to call configDidLoad,
    // Config, plugin files are referred,
    // this is the last chance to modify the config.
  }
  // 配置文件加载完成
  configDidLoad() {
    // Config, plugin files have been loaded.
  }
  // 文件加载完成
  async didLoad() {
    // All files have loaded, start plugin here.
  }
  // 插件启动完毕
  async willReady() {
    // All plugins have started, can do some thing before app ready
  }
  // worker 准备就绪
  async didReady() {
    // 请将您的 app.beforeStart 中的代码置于此处。
  }
  // 应用启动完成
  async serverDidReady() {
    // Server is listening.
  }
  // 应用即将关闭
  async beforeClose() {
    // 请将您的 app.beforeClose 中的代码置于此处。
  }

}

module.exports = AppBootHook;
